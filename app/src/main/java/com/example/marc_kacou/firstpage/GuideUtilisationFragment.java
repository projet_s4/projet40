package com.example.marc_kacou.firstpage;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Marc-Kacou on 07/05/2015.
 * Guide d'utilisation de l'application mobile.
 * Pour modifier son comportement, il faut ouvrir le fichier XML associé.
 */
public class GuideUtilisationFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.guide_utilisation, container, false);

        return rootView;
    }

}
