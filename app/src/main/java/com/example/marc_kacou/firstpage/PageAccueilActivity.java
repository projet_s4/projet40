package com.example.marc_kacou.firstpage;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

/**
 * Page d'accueil de l'application mobile.
 */
public class PageAccueilActivity extends Activity {

    /**
     *  Pour garder en memoire le mode choisi par l'utilisateur
     *  Pour lancer l'application
      */
    public final static String MODE_CHOISI = "Identifier";
    private static final int RA_ACTIVITY_NUM = 0;
    private static final int MAP_FRAGMENT_NUM = 1;
    private static final int GUIDE_FRAGMENT_NUM = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_accueil);

        // Initialisation des 4 boutons pour lancer l'application

        ImageButton btn_ar = (ImageButton)findViewById(R.id.btn_ar);
        btn_ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PageAccueilActivity.this,ModePageActivity.class);
                intent.putExtra(PageAccueilActivity.MODE_CHOISI, RA_ACTIVITY_NUM);
                startActivity(intent);
                PageAccueilActivity.this.finish();

                 }
        });

        ImageButton btn_carte = (ImageButton)findViewById(R.id.btn_carte);
        btn_carte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PageAccueilActivity.this,ModePageActivity.class);
                intent.putExtra(PageAccueilActivity.MODE_CHOISI, MAP_FRAGMENT_NUM);
                startActivity(intent);
                PageAccueilActivity.this.finish();

            }
        });

        ImageButton btn_guide = (ImageButton)findViewById(R.id.btn_guide);
        btn_guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PageAccueilActivity.this,ModePageActivity.class);
                intent.putExtra(PageAccueilActivity.MODE_CHOISI, GUIDE_FRAGMENT_NUM);
                startActivity(intent);
                PageAccueilActivity.this.finish();

            }
        });

        ImageButton btn_community = (ImageButton)findViewById(R.id.btn_community);
        btn_community.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    public void run() {
                        Uri site = Uri.parse(getString(R.string.facebook));
                        Intent mIntent = new Intent(Intent.ACTION_VIEW, site);
                        startActivity(mIntent);
                    }
                }).start();

            }
        });

    }

    @Override
    public void onBackPressed(){
       finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
