package com.example.marc_kacou.firstpage.elements;

/**
 * Created by Cong on 5/15/2015.
 */
public class Type {
    private int id;
    private String titre;
    private String date;

    public Type(int id, String titre, String date) {
        this.id = id;
        this.titre = titre;
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getDate() {
        return date;
    }
}
