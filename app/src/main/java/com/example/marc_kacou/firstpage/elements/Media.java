package com.example.marc_kacou.firstpage.elements;

/**
 * Created by Cong on 5/15/2015.
 */
public class Media {
    private int id;
    private String lien;
    private int nature;
    private int idPdi;
    private String description;

    public Media(int id, String lien, int nature, int idPdi, String description) {
        this.id = id;
        this.lien = lien;
        this.nature = nature;
        this.idPdi = idPdi;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getLien() {
        return lien;
    }

    public int getNature() {
        return nature;
    }

    public int getIdPdi() {
        return idPdi;
    }

    public String getDescription() {
        return  description;
    }
}
