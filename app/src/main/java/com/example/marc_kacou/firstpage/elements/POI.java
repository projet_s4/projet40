package com.example.marc_kacou.firstpage.elements;

import android.util.SparseArray;

import com.google.android.gms.maps.model.LatLng;

import java.sql.Date;
import java.util.HashMap;

/**
 * Created by Marc-Kacou on 20/04/2015.
 */
public class POI {

    private int id;
    private LatLng latlng;
    private double altitude;
    private String nom;
    private String description;
    private boolean affichage;
    private String date;
    private Type type;
    private SparseArray<Media> medias;
    private int icone_id;


    public POI(int id, String nom, double latitude, double longitude, double altitude, String description, boolean affichage, String date, Type type){
        this.id = id;
        this.latlng = new LatLng(latitude, longitude);
        this.altitude = altitude;
        this.nom = nom;
        this.description = description;
        this.affichage = affichage;
        this.date = date;
        this.type = type;

        this.medias = new SparseArray<Media>();
    }

    public int getIdPOI() {
        return id;
    }

    public LatLng getLatLng() {
        return latlng;
    }

    public double getAltitude() {
        return altitude;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public Type getType() {
        return type;
    }

    public boolean isAffichage() {
        return affichage;
    }

    public String getDate() {
        return date;
    }

    public int getIcone_id() {
        return icone_id;
    }

    public void addMedia(Media media){
        medias.put(media.getId(), media);
    }

    public SparseArray<Media> getMedias() {
        return medias;
    }
}
